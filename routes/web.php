<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    
Route::get('/genre/create','GenreController@create');
Route::post('/genre','GenreController@store');
Route::get('/genre','GenreController@index');
Route::get('/genre/{genre_id}','GenreController@show');
Route::delete('/genre/{genre_id}','GenreController@destroy');

Route::resource('komentar','KomentarController')->only([
    'store'
]);

//profil
Route::resource('profil','ProfilController')->only([

    'index','update'
    ]);


});




Route::resource('buku','BukuController');



