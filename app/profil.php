<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $table = 'profil';
    protected $fillable = ['umur','bio','user_id'];

    public $timestamps = false;


public function user()
    {
        return $this->belongsTo('App\User');
    }

}