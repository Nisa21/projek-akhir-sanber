<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
       
        $request->validate([
            'jenis_genre' => 'required',
        ]);
    
        DB::table('genre')->insert(
            [
                'jenis_genre' => $request['jenis_genre']
            ]
        );
    
        return redirect('/genre');
    }

    public function index(){
        $genre  = DB::table('genre')->get();

        return view('genre.index', compact('genre'));
    }

    public function show($id)
    {
        $genre = Genre::find($id);

        return view('genre.show', compact('genre'));

    }

    public function destroy($id)
    {
        DB::table('genre')->where('id', '=', $id)->delete();
        return redirect('/genre');
    }




}
