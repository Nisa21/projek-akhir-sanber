<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Genre;
use File;

class BukuController extends Controller
{

    public function __construct(){

        $this->middleware('auth')->except('index','show');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::get();
        return view('buku.index', compact('buku'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::get();
        return view('buku.create', compact('genre'));
    }
        
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'deskripsi' => 'required',
            'penulis' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
         
        ]);

        $imageName = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('images'), $imageName);
        
        $buku = new Buku;

        $buku->judul = $request->judul;
        $buku->deskripsi = $request->deskripsi;
        $buku->penulis = $request->penulis;
        $buku->tahun = $request->tahun;
        $buku->genre_id = $request->genre_id;
        $buku->thumbnail = $imageName;

        $buku->save();

        return redirect('/buku');

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $buku = Buku::find($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::get();
        $buku = Buku::find($id);
        return view('buku.edit', compact('buku','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'deskripsi' => 'required',
            'penulis' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
         
        ]);

        
        $buku = Buku::findorfail($id);

        if($request->has('thumbnail')){

            $path = "images/";
            File::delete($path . $buku->thumbnail);
            $imageName = time().'.'.$request->thumbnail->extension();
            $request->thumbnail->move(public_path('images/'),$imageName);
            $buku_data = [
                'judul' =>  $request->judul,
                'deskripsi' =>  $request->deskripsi,
                'penulis' =>  $request->penulis,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,
                'thumbnail' => $imageName
            ];
        }else{
            $buku_data = [
                'judul' =>  $request->judul,
                'deskripsi' =>  $request->deskripsi,
                'penulis' =>  $request->penulis,
                'tahun' => $request->tahun,
                'genre_id' => $request->genre_id,

            ];
        }

        $buku->update($buku_data);

      

        $buku->save();

        return redirect('/buku');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::findorfail($id);
        $buku->delete();

        $path="images/";
        File::delete($path . $buku->thumbnail);

        return redirect('/buku');
    }
}
