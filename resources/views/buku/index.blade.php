@extends('layout.main')
@section('judul')
    Daftar Buku
@endsection

@section('content')

<link rel="stylesheet" href="{{asset('admin/plugins/sweetalert/sweetalert2.min.css')}}">

@auth
<a href="/buku/create" class="btn btn-primary btn-sm mt-2 mb-4">Tambah</a>
    
@endauth


<div class="row">
    @forelse ($buku as $item)
    <div class="col-3">
        <div class="card" >
            <img src="{{asset('images/'.$item->thumbnail)}}" class="card-img-top"  alt="...">
            <div class="card-body">
              <h5>{{$item->judul}}</h5>

              <span class="badge badge-light mb-2">{{$item->genre->jenis_genre}}</span>
              <p class="card-text">{{Str::limit($item->deskripsi,50)}}</p>
              @auth
              <form action="/buku/{{$item->id}}" method="POST">
                @method('DELETE')
                @csrf
                <a href="/buku/{{$item->id}}" class="btn btn-sm" style="background-color: #98FD8E">Read More</a>
                <a href="/buku/{{$item->id}}/edit" class="btn btn-sm" style="background-color: #56CFF4">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm"  value="DELETE">
            </form>
  
                  
              @endauth

              @guest
              <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Read More</a>
              @endguest
            </div>
          </div>
    </div>
        
    @empty
        <h5>Tidak ada Buku</h5>
    @endforelse



</div>

<script src="{{asset('admin/plugins/sweetalert/sweetalert2.min.js')}}"></script>
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Yeay",
    });
</script>


@endsection



    



