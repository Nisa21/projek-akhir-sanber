<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>Dtox</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{asset('template/plugins/bootstrap/bootstrap.min.css')}}">
  <!-- themefy-icon -->
  <link rel="stylesheet" href="{{asset('template/plugins/themify-icons/themify-icons.css')}}">
  <!-- slick slider -->
  <link rel="stylesheet" href="{{asset('template/plugins/slick/slick.css')}}">
  <!-- venobox popup -->
  <link rel="stylesheet" href="{{asset('template/plugins/Venobox/venobox.css')}}">
  <!-- aos -->
  <link rel="stylesheet" href="{{asset('template/plugins/aos/aos.css')}}">

  <!-- Main Stylesheet -->
  <link href="{{asset('template/css/style.css')}}" rel="stylesheet">
  
  <!--Favicon-->
  <link rel="shortcut icon" href="{{asset('template/images/favicon.ico')}}" type="image/x-icon">
  <link rel="icon" href="{{asset('template/images/favicon.ico')}}" type="image/x-icon">

</head>

<body>
  

<!-- navigation -->
<section class="fixed-top navigation">
  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light">
      <a class="navbar-brand" href="index.html"><img src="{{asset('admin/dist/img/book_logo.png')}}" alt="logo"></a>
      <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- navbar -->
      <div class="collapse navbar-collapse text-center" id="navbar">
        <ul class="navbar-nav ml-auto">
        
      
        </ul>
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/buku') }}" class="btn btn-primary ml-lg-2 primary-shadow">Home</a>
            @else
            <a href="{{ route('login') }}" class="btn btn-primary ml-lg-2 primary-shadow">Login</a>

                @if (Route::has('register'))
                <a href="{{ route('register') }}" class="btn btn-primary ml-lg-2 primary-shadow">Register</a>
                @endif
            @endauth
        </div>
    @endif
      </div>
    </nav>
  </div>
</section>
<!-- /navigation -->

<!-- hero area -->
<section class="hero-section hero" data-background="" style="background-image: url({{asset('template/images/hero-area/banner-bg.png')}});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center zindex-1">
        <h1 class="mb-3">Temukan Buku dengan <br>
        Kisah yang Menarik</h1>
        <p class="mb-4">Temukan buku favoritmu dan <br>bagikan ulasan-ulasanmu untuk buku kesukaanmu <br></p>
        <a href="/buku" class="btn btn-secondary btn-lg">Lihat Buku</a>

        
      </div>
    </div>
  </div>
  <!-- background shapes -->
  <div id="scene">
    <img class="img-fluid hero-bg-1 up-down-animation" src="{{asset('template/images/background-shape/feature-bg-2.png')}}" alt="">
    <img class="img-fluid hero-bg-2 left-right-animation"src="{{asset('template/images/background-shape/seo-ball-1.png')}}" alt="">
    <img class="img-fluid hero-bg-3 left-right-animation" src="{{asset('template/images/background-shape/seo-half-cycle.png')}}" alt="">
    <img class="img-fluid hero-bg-4 up-down-animation" src="{{asset('template/images/background-shape/green-dot.png')}}" alt="">
    <img class="img-fluid hero-bg-5 left-right-animation" src="{{asset('template/images/background-shape/blue-half-cycle.png')}}" alt="">
    <img class="img-fluid hero-bg-6 up-down-animation" src="{{asset('template/images/background-shape/seo-ball-1.png')}}" alt="">
    <img class="img-fluid hero-bg-7 left-right-animation" src="{{asset('template/images/background-shape/yellow-triangle.png')}}" alt="">
    <img class="img-fluid hero-bg-8 up-down-animation" src="{{asset('template/images/background-shape/service-half-cycle.png')}}" alt="">
    <img class="img-fluid hero-bg-9 up-down-animation" src="{{asset('template/images/background-shape/team-bg-triangle.png')}}" alt="">
  </div>
</section>
<!-- /hero-area -->







</body>
</html>