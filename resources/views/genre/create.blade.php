@extends('layout.main')
@section('judul')
    Tambah Buku
@endsection

@section('content')

        <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label >Jenis Genre</label>
                <input type="text" class="form-control" name="jenis_genre" placeholder="Masukkan Jenis Genre">
                @error('jenis_genre')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
    
@endsection